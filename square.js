function CalculateSquareArea(x) {
    x = Number(x);
    return Math.pow(x,2);
}


function CalculateSquareRound(x){
    x = Number(x);
    return 4 * x;
  
}

module.exports = {
    CalculateSquareArea,
    CalculateSquareRound
}
