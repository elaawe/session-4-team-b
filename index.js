// Require function will allow you to call the module
// This is from index.js
const readline = require('readline');
const square = require('./square.js');
const triangle = require('./triangle.js');
const cube = require('./cube.js');
const circle = require('./circle.js');

// Initiate the readline to use stdin and stdout as input and output
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Start up message
console.clear(); // To clear up the terminal
console.log("Which one do you want to calculate?");
console.log(`
1. Square 
2. Triangle
3. Cube
4. Circle
`)

// This is used to match non-digits characters
let noNumRegex = /\D/g;

// This function to handle validation before calculating square area
function squareArea () {
    console.clear();
    console.log("Calculate Your Square Area");

    rl.question("X: ", x => {
        // If there's a non-digit in x
        // Then it will raise an error
        if(x.match(noNumRegex)){
        console.log('It should be number only');
        rl.close();
        }else{
            console.log('The result is: ',
            square.CalculateSquareArea(x));
            rl.close();
        }
    })
}
// This function to handle validation before calculating square round 
function squareRound () {
    console.clear();
    console.log("Calculate Your Square Round");

    // Ask for the height
    rl.question("X: ", x => {
        
        // If there's a digit in height
        // Then it will raise an error
        if(x.match(noNumRegex)){
        console.log('It should be number only');
        rl.close();
        }else{
        console.log(
            "The result is: ",
            square.CalculateSquareArea(x) 
        )
        rl.close();
        }
        
    })
}
// This function to handle validation before calculating triangle area
function triangleArea () {
    console.clear();
    console.log("Calculate Your Triangle Area");
    rl.question("H: ", H => {
        // If there's a non-digit in x
        // Then it will raise an error
        if(H.match(noNumRegex)){
            console.log('It should be number only');
        rl.close();
        }else{
            rl.question("B: ", B => {
                if(B.match(noNumRegex)){
                    console.log('It should be number only');
                    rl.close();
                }else{
                    console.log('The result is: ',
                    triangle.triangleArea(H,B));
                    rl.close();
                }  
            })
        }
    })
}
// This function to handle validation before calculating triangle round
function triangleRound () {
    console.clear();
    console.log("Calculate Your Triangle Round");

    rl.question("S1: ", s1 => {
        // If there's a non-digit in x
        // Then it will raise an error
        if(s1.match(noNumRegex)){
            console.log('It should be number only');
        rl.close();
        }else{
            rl.question("S2: ", s2 => {
                if(s2.match(noNumRegex)){
                    console.log('It should be number only');
                    rl.close();
                }else{
                    rl.question("S3: ", s3 => {
                        if(s3.match(noNumRegex)){
                            console.log('It should be number only');
                            rl.close();
                        }else{
                            console.log('The result is: ',
                            triangle.triangleRound(s1,s2,s3));
                            rl.close();
                        }
                    })
                    
                }  
            })
        }
    })
}
// This function to handle validation before calculating circle area
function circleArea () {
    console.clear();
    console.log("Calculate Your Circle Area");

    rl.question("R: ", r => {
        // If there's a non-digit in x
        // Then it will raise an error
        if(r.match(noNumRegex)){
        console.log('It should be number only');
        rl.close();
        }else{
            console.log('The result is: ',
            circle.calculateCircleArea(r));
            rl.close();
        }
    })
}
// This function to handle validation before calculating circle round
function circleRound () {
    console.clear();
    console.log("Calculate Your Circle Round");

    // Ask for the diameter
    rl.question("D: ", d => {
        
        // If there's a digit in diameter
        // Then it will raise an error
        if(d.match(noNumRegex)){
        console.log('It should be number only');
        rl.close();
        }else{
        console.log(
            "The result is: ",
            circle.calculateCircleRound(d) 
        )
        rl.close();
        }
        
    })
}


// This function handle if user choose 1 
function ifIsSquare() {
  console.clear();
  console.log('Area or Round??? ')
  console.log('1. Area', '\n2. Round');
  rl.question('Your choice: ', answer => {
    switch(+answer) {
        case 1:
            squareArea();
            break;
        case 2:
            squareRound();
            break;
        default:
            console.log("Sorry, there's no option for that!")
            rl.close();
    }
  })
}
// This function handle if user choose 2
function ifIsTriangle() {
    console.clear();
    console.log('Area or Round??? ')
    console.log('1. Area', '\n2. Round');
    rl.question('Your choice: ', answer => {
      switch(+answer) {
          case 1:
              triangleArea();
              break;
          case 2:
              triangleRound();
              break;
          default:
              console.log("Sorry, there's no option for that!")
              rl.close();
      }
    })
  }

// This function handle if user choose 3
// This is used to handle validation before calculating cube's volume  
function ifIsCube() {
    console.clear();
    console.log('Calculate Cube\'s volume ')
    rl.question('Your input: ', s => {
      if(s.match(noNumRegex)) {
          console.log('It should be a number');
          rl.close();
      }else{
        console.log(
        "The result is: ",
        cube.CalculateVolum(s) 
        )
        rl.close();
      }
    })
}
// This function handle if user choose 4
function ifIsCircle() {
    console.clear();
    console.log('Area or Round??? ')
    console.log('1. Area', '\n2. Round');
    rl.question('Your choice: ', answer => {
    switch(+answer) {
        case 1:
            circleArea();
            break;
        case 2:
            circleRound();
            break;
        default:
            console.log("Sorry, there's no option for that!")
            rl.close();
    }
  })
}



rl.question('Answer: ', answer => {


    // +answer to convert string into number
    switch(+answer) {
        case 1:
            ifIsSquare();
            break;
        case 2:
            ifIsTriangle();
            break;
        case 3:
            ifIsCube();
             break;
        case 4:
            ifIsCircle();
            break;
        default:
            console.log("Sorry, there's no option for that!")
            rl.close();

    }
});
