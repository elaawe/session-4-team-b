
const phi = 3.14;

function calculateCircleArea(r){
	r = Number(r);
	return 2*phi*r;
}

function calculateCircleRound(d){
	d = Number(d);
	return phi*d;
}

module.exports ={
	calculateCircleArea,
	calculateCircleRound
}
