function triangleArea(H, B) {
    return 0.5 * H * B;
}



function triangleRound(s1, s2, s3) {
    s1 = Number(s1);
    s2 = Number(s2);
    s3 = Number(s3);
    return (s1 + s2 + s3);
}

module.exports = {
    triangleArea,
    triangleRound
};